===============
python_lemonway
===============

Python API to access Lemonway web services


Installation
============

Install it with ``pip``:

    pip install git+https://bitbucket.org/payplug/python-lemonway.git
